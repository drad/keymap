# README

## Purpose ##
A simple key mapper - display a given key mapping.

You call keymap with a key, it returns you a simple key mapping.

e.g. https://apps.dradux.com/keymap/index.html?k=test1 will return the Test 1 keymap shown below.

![alt text](docs/example-test-1.png "Test 1 keymap example")


## Architeture ##
This app was designed to be basic, using html and js only. The 'app data', which defines all key mappings, can/should be mounted into your container as a volume which allows changing the data without requiring an image update.

Formal Stack
* lighttpd: web server handling html/css/js

The application has been dockerized, you can run it via docker-compose.

The application has been designed to run in k8s with the 'app data' stored in a ConfigMap, see app/k8s/* for details.

There is no observer so changes to the ConfigMap require a restart of the instance(s).

You can also run the app via your browser locally (no http server) although I'm not sure why this would be useful.


### Deployment

#### k8s/helm

This application is fully deployable via helm, go to the `.devops/helm` directory and perform the following to deploy:

- configure the `values.yaml.prod` file per your needs
- deploy the chart: `helm install keymap keymap/ --values keymap/values.yaml.prod --create-namespace --namespace keymap --disable-openapi-validation`
    - uninstall: `helm uninstall keymap -n keymap`
