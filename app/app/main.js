/*
 * the keymap data logic.
 *   note: version is stored in version.json as it is served via lighttpd.
 */
var year = new Date().getFullYear()
function load() {
    var url = new URL(window.location.href);
    var keymap = url.searchParams.get("k")
    var mydata = window[keymap];
    var table = document.getElementById("dataTable");
    if ( typeof mydata === "object" && mydata.keys && mydata.keys.length > 0 ) {
        document.getElementById("data-title").innerHTML = mydata.title;
        document.getElementById("keymap-modified").innerHTML = "modified " + mydata.modified;
        dataList = mydata.keys.sort((a, b) => (a.action > b.action) ? 1 : -1)
        for(var i = 0;i < dataList.length; i++) {
             var row = table.insertRow(i+1);   // +1 for header row
             var key = row.insertCell(0);
             key.innerHTML = dataList[i].key;
             var action = row.insertCell(1);
             action.innerHTML = dataList[i].action;
             var note = row.insertCell(2);
             note.innerHTML = dataList[i].note;
        }
    } else {
        // no data found.
        document.getElementById("data-title").innerHTML = "Not Data Found for: " + keymap;
        document.getElementById("keymap-modified").innerHTML = "modified n/a";
        var row = table.insertRow(1);   // +1 for header row
        var key = row.insertCell(0);
        key.innerHTML = "-";
        var action = row.insertCell(1);
        action.innerHTML = "-";
        var note = row.insertCell(2);
        note.innerHTML = "-";
    }
    // set version
    document.getElementById("app-version").innerHTML = "v." + app_version;
    document.getElementById("copyright").innerHTML = "<a href='dradux.com'>&copy; " + year + " dradux.com</a>";
}
